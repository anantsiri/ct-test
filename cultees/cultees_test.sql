-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2018 at 07:46 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cultees_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `geo_provinces`
--

CREATE TABLE `geo_provinces` (
  `PROVINCE_ID` int(11) NOT NULL,
  `PROVINCE_NAME` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `geo_provinces`
--

INSERT INTO `geo_provinces` (`PROVINCE_ID`, `PROVINCE_NAME`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'กรุงเทพมหานคร', '2018-04-25 11:34:00', NULL, NULL),
(2, 'สมุทรปราการ', '2018-04-25 11:34:00', NULL, NULL),
(3, 'นนทบุรี', '2018-04-25 11:34:00', NULL, NULL),
(4, 'ปทุมธานี', '2018-04-25 11:34:00', NULL, NULL),
(5, 'พระนครศรีอยุธยา', '2018-04-25 11:34:00', NULL, NULL),
(6, 'อ่างทอง', '2018-04-25 11:34:00', NULL, NULL),
(7, 'ลพบุรี', '2018-04-25 11:34:00', NULL, NULL),
(8, 'สิงห์บุรี', '2018-04-25 11:34:00', NULL, NULL),
(9, 'ชัยนาท', '2018-04-25 11:34:00', NULL, NULL),
(10, 'สระบุรี', '2018-04-25 11:34:00', NULL, NULL),
(11, 'ชลบุรี', '2018-04-25 11:34:00', NULL, NULL),
(12, 'ระยอง', '2018-04-25 11:34:00', NULL, NULL),
(13, 'จันทบุรี', '2018-04-25 11:34:00', NULL, NULL),
(14, 'ตราด', '2018-04-25 11:34:00', NULL, NULL),
(15, 'ฉะเชิงเทรา', '2018-04-25 11:34:00', NULL, NULL),
(16, 'ปราจีนบุรี', '2018-04-25 11:34:00', NULL, NULL),
(17, 'นครนายก', '2018-04-25 11:34:00', NULL, NULL),
(18, 'สระแก้ว', '2018-04-25 11:34:00', NULL, NULL),
(19, 'นครราชสีมา', '2018-04-25 11:34:00', NULL, NULL),
(20, 'บุรีรัมย์', '2018-04-25 11:34:00', NULL, NULL),
(21, 'สุรินทร์', '2018-04-25 11:34:00', NULL, NULL),
(22, 'ศรีสะเกษ', '2018-04-25 11:34:00', NULL, NULL),
(23, 'อุบลราชธานี', '2018-04-25 11:34:00', NULL, NULL),
(24, 'ยโสธร', '2018-04-25 11:34:00', NULL, NULL),
(25, 'ชัยภูมิ', '2018-04-25 11:34:00', NULL, NULL),
(26, 'อำนาจเจริญ  ', '2018-04-25 11:34:00', NULL, NULL),
(27, 'หนองบัวลำภู', '2018-04-25 11:34:00', NULL, NULL),
(28, 'ขอนแก่น', '2018-04-25 11:34:00', NULL, NULL),
(29, 'อุดรธานี', '2018-04-25 11:34:00', NULL, NULL),
(30, 'เลย', '2018-04-25 11:34:00', NULL, NULL),
(31, 'หนองคาย', '2018-04-25 11:34:00', NULL, NULL),
(32, 'มหาสารคาม', '2018-04-25 11:34:00', NULL, NULL),
(33, 'ร้อยเอ็ด', '2018-04-25 11:34:00', NULL, NULL),
(34, 'กาฬสินธุ์', '2018-04-25 11:34:00', NULL, NULL),
(35, 'สกลนคร', '2018-04-25 11:34:00', NULL, NULL),
(36, 'นครพนม', '2018-04-25 11:34:00', NULL, NULL),
(37, 'มุกดาหาร', '2018-04-25 11:34:00', NULL, NULL),
(38, 'เชียงใหม่', '2018-04-25 11:34:00', NULL, NULL),
(39, 'ลำพูน', '2018-04-25 11:34:00', NULL, NULL),
(40, 'ลำปาง', '2018-04-25 11:34:00', NULL, NULL),
(41, 'อุตรดิตถ์', '2018-04-25 11:34:00', NULL, NULL),
(42, 'แพร่', '2018-04-25 11:34:00', NULL, NULL),
(43, 'น่าน', '2018-04-25 11:34:00', NULL, NULL),
(44, 'พะเยา', '2018-04-25 11:34:00', NULL, NULL),
(45, 'เชียงราย', '2018-04-25 11:34:00', NULL, NULL),
(46, 'แม่ฮ่องสอน', '2018-04-25 11:34:00', NULL, NULL),
(47, 'นครสวรรค์', '2018-04-25 11:34:00', NULL, NULL),
(48, 'อุทัยธานี', '2018-04-25 11:34:00', NULL, NULL),
(49, 'กำแพงเพชร', '2018-04-25 11:34:00', NULL, NULL),
(50, 'ตาก', '2018-04-25 11:34:00', NULL, NULL),
(51, 'สุโขทัย', '2018-04-25 11:34:00', NULL, NULL),
(52, 'พิษณุโลก', '2018-04-25 11:34:00', NULL, NULL),
(53, 'พิจิตร', '2018-04-25 11:34:00', NULL, NULL),
(54, 'เพชรบูรณ์', '2018-04-25 11:34:00', NULL, NULL),
(55, 'ราชบุรี', '2018-04-25 11:34:00', NULL, NULL),
(56, 'กาญจนบุรี', '2018-04-25 11:34:00', NULL, NULL),
(57, 'สุพรรณบุรี', '2018-04-25 11:34:00', NULL, NULL),
(58, 'นครปฐม', '2018-04-25 11:34:00', NULL, NULL),
(59, 'สมุทรสาคร', '2018-04-25 11:34:00', NULL, NULL),
(60, 'สมุทรสงคราม', '2018-04-25 11:34:00', NULL, NULL),
(61, 'เพชรบุรี', '2018-04-25 11:34:00', NULL, NULL),
(62, 'ประจวบคีรีขันธ์', '2018-04-25 11:34:00', NULL, NULL),
(63, 'นครศรีธรรมราช', '2018-04-25 11:34:00', NULL, NULL),
(64, 'กระบี่', '2018-04-25 11:34:00', NULL, NULL),
(65, 'พังงา', '2018-04-25 11:34:00', NULL, NULL),
(66, 'ภูเก็ต', '2018-04-25 11:34:00', NULL, NULL),
(67, 'สุราษฎร์ธานี', '2018-04-25 11:34:00', NULL, NULL),
(68, 'ระนอง', '2018-04-25 11:34:00', NULL, NULL),
(69, 'ชุมพร', '2018-04-25 11:34:00', NULL, NULL),
(70, 'สงขลา', '2018-04-25 11:34:00', NULL, NULL),
(71, 'สตูล', '2018-04-25 11:34:00', NULL, NULL),
(72, 'ตรัง', '2018-04-25 11:34:00', NULL, NULL),
(73, 'พัทลุง', '2018-04-25 11:34:00', NULL, NULL),
(74, 'ปัตตานี', '2018-04-25 11:34:00', NULL, NULL),
(75, 'ยะลา', '2018-04-25 11:34:00', NULL, NULL),
(76, 'นราธิวาส', '2018-04-25 11:34:00', NULL, NULL),
(77, 'บึงกาฬ', '2018-04-25 11:34:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_25_202701_create_sessions_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('VwssN2EnTJCISdVMVk0F4egMqLHQobXDw6Q1RFUc', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'YTo0OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoib2p2Z2pmb0xSajRQMHNGbktEZnlTNmxIaEJMOWNYMmQzUTlYMUpVMCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjM6Imh0dHA6Ly9jdWx0ZWVzLmxvY2FsL3BpIjt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTt9', 1524678242);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','unspecific') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unspecific',
  `birthdate` date DEFAULT NULL,
  `address_line` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_subdist` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_dist` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_province` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_zipcode` int(5) DEFAULT NULL,
  `phone` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `surname`, `gender`, `birthdate`, `address_line`, `address_subdist`, `address_dist`, `address_province`, `address_zipcode`, `phone`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'anantsiri', 'anantsiri@anantsiri.com', '$2y$10$0Nmyf4nm2EywRH0RTsqu7e021T6ZNdzOXFk/iD5FmWv3YG9osSS86', 'choeypho', 'male', '1987-08-15', NULL, NULL, NULL, 'กาญจนบุรี', NULL, NULL, 'admin', 'f6G8XTrdcZqBqNXozdEgpDtiDZb5nmFAhsBDpp0LGhIFSADc0YIlrJ5w3pEC', '2018-04-25 13:47:54', '2018-04-25 17:34:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `geo_provinces`
--
ALTER TABLE `geo_provinces`
  ADD PRIMARY KEY (`PROVINCE_ID`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `geo_provinces`
--
ALTER TABLE `geo_provinces`
  MODIFY `PROVINCE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
