<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\GeoProvince;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $province_list = GeoProvince::orderBy('PROVINCE_NAME')->pluck('PROVINCE_NAME', 'PROVINCE_NAME');
        $users = User::all();
        return view('admin.user.index', compact('users','province_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        );
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::to(route('admin.user.index'))
                ->withErrors($validator)
                ->withInput();
        } else {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            return \Redirect::to(route('admin.user.index').'#tr-'.$user->id)->with('message', 'เพิ่ม user สำเร็จ');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = array(
            'name' => 'required',
            'email' => 'required|string|email|max:255',
            'password' => 'string|min:6|confirmed|nullable',
            'birthdate' => 'date|nullable',
            'address_zipcode' => 'numeric|nullable',
            'gender' => 'in:unspecific,female,male',
            'role' => 'in:admin,user'
        );
        if (!empty($request->email) && trim($request->email) != trim($user->email)) {
            $rules['email'] = 'required|string|email|max:255|unique:users';
        }
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'error_detail' => $validator->messages()], 200);
        } else {
            $user->update([
                'name' => $request->name,
                'surname' => $request->surname,
                'email' => $request->email,
                'gender' => $request->gender,
                'address_line' => $request->address_line,
                'address_subdist' => $request->address_subdist,
                'address_dist' => $request->address_dist,
                'address_province' => $request->address_province,
                'phone' => $request->phone,
                'role' => $request->role,
                'birthdate' => $request->birthdate,
                'address_zipcode' => $request->address_zipcode
            ]);
            if (!empty($request->password)) {
                $user->password = Hash::make($request->password);
                $user->save();
            }
            return response()->json(['error' => false, 'user_id' => $user->id, 'user_detail' => $user->toArray()], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user_id = $user->id;
        $user->delete();
        return response()->json(['error' => false, 'user_id' => $user_id], 200);
    }
}
