<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * Check if the user is admin
     *
     * TODO: check for additional user types - eg. staff, moderators etc
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect('/login');
        } elseif (Auth::check()) {


            $isAdmin = false;
            $user = Auth::user();

            if (!is_null($user)) {
                $isAdmin = $user->role == 'admin' ? true : false;
            }

            if ($isAdmin == false) {
                Session::flash('alert-danger', 'เฉพาะสิทธิ์ Admin เท่านั้น');
                return redirect('/');
            }
        }

        return $next($request);
    }
}
