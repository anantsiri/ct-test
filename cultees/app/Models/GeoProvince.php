<?php

namespace App\Models;

class GeoProvince extends \App\Models\Base\GeoProvince
{
	protected $fillable = [
		'PROVINCE_NAME'
	];
}
