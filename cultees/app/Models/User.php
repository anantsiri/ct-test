<?php

namespace App\Models;

class User extends \App\Models\Base\User
{
	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'surname',
		'gender',
		'birthdate',
		'address_line',
		'address_subdist',
		'address_dist',
		'address_province',
		'address_zipcode',
		'phone',
		'role',
		'remember_token'
	];
}
