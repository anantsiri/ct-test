<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 25 Apr 2018 20:40:26 +0700.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GeoProvince
 * 
 * @property int $PROVINCE_ID
 * @property string $PROVINCE_NAME
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models\Base
 */
class GeoProvince extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'PROVINCE_ID';
}
