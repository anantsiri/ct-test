<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 25 Apr 2018 20:40:26 +0700.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $surname
 * @property string $gender
 * @property \Carbon\Carbon $birthday
 * @property string $address_line
 * @property string $address_subdist
 * @property string $address_dist
 * @property string $address_province
 * @property int $address_zipcode
 * @property string $phone
 * @property string $role
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models\Base
 */
class User extends Eloquent
{
	protected $casts = [
		'address_zipcode' => 'int'
	];

	protected $dates = [
		'birthdate'
	];
}
