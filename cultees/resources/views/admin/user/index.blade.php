@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">เพิ่ม User ใหม่</div>

                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <form method="POST" action="{{ route('admin.user.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">ชื่อ</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">อีเมล</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">รหัสผ่าน</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">รหัสผ่านอีกครั้ง</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    เพิ่ม
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="container" id="adminUserContainner">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">User ทั้งหมด</div>

                <div class="card-body">
                  <!-- will be used to show any messages -->

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>รายละเอียด</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $key => $user)
                        <tr class="tr-{{$user->id}}" id="tr-{{$user->id}}">
                            <td>{{ $user->id }}</td>
                            <td>
                            <form>
                            <table class="table">
                                <tbody>
                                <tr>
                                <td>อีเมล</td>
                                <td>{{ Form::text('email', $user->email, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-email-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>ชื่อ</td>
                                <td>{{ Form::text('name', $user->name, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-name-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>นามสกุล</td>
                                <td>{{ Form::text('surname', $user->surname, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-surname-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>เพศ</td>
                                <td>{{Form::select('gender', ['female' => 'หญิง','male' => 'ชาย','unspecific' => 'ไม่ระบุ' ],$user->gender, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-gender-'.$user->id,'disabled'=>'disabled'))}}</td>
                                </tr>

                                <tr>
                                <td>วันเกิด</td>
                                <td>{{ Form::date('birthdate', $user->birthdate, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-birthdate-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>ที่อยู่</td>
                                <td>{{ Form::text('address_line', $user->address_line, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-address_line-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>ตำบล/แขวง</td>
                                <td>{{ Form::text('address_subdist', $user->address_subdist, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-address_subdist-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>อำเภอ/เขต</td>
                                <td>{{ Form::text('address_dist', $user->address_dist, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-address_dist-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>จังหวัด</td>
                                <td>{{ Form::select('address_province', $province_list, $user->address_province, array('placeholder' => 'เลือกจังหวัด', 'class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-address_province-'.$user->id,'disabled'=>'disabled'))}}</td>
                                </tr>

                                <tr>
                                <td>รหัสไปรษณีย์</td>
                                <td>{{ Form::text('address_zipcode', $user->address_zipcode, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-address_zipcode-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>เบอร์โทรศัพท์</td>
                                <td>{{ Form::text('phone', $user->phone, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-phone-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>สิทธิ์</td>
                                <td>{{Form::select('role', ['user' => 'user','admin' => 'admin' ],$user->role, array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-role-'.$user->id,'disabled'=>'disabled'))}}</td>
                                </tr>

                                <tr>
                                <td>รหัสผ่านใหม่ (ปล่อยว่างในกรณีไม่ต้องการเปลี่ยน)</td>
                                <td>{{ Form::password('password',array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-password-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>

                                <tr>
                                <td>รหัสผ่านใหม่อีกครั้ง</td>
                                <td>{{ Form::password('password_confirmation', array('class' => 'form-control form-detail-'.$user->id,'id'=>'form-detail-password_confirmation-'.$user->id,'disabled'=>'disabled')) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <a class="btn btn-small btn-success btn-update  btn-update-{{$user->id}}" style="display:none" data-user-id="{{$user->id}}" href="#">Update</a>
                                        <a class="btn btn-small  btn-secondary btn-cancel btn-cancel-{{$user->id}}" style="display:none" data-user-id="{{$user->id}}" href="#">Cancel</a>
                                        <a class="btn btn-small btn-info btn-edit btn-edit-{{$user->id}}" data-user-id="{{$user->id}}"  href="#">Edit</a>
                                        <a class="btn btn-small btn-danger btn-delete" data-user-id="{{$user->id}}"  href="#">Delete</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<style>

</style>

@endsection

@section('script')
<script>
jQuery(document).ready(function($) {  
    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    if (window.location.hash && isChrome) {
        setTimeout(function () {
            var hash = window.location.hash;
            window.location.hash = "";
            window.location.hash = hash;
        }, 300);
    }
    $(".btn-edit").on('click',function(e){
        e.preventDefault();
        var user_id = $(this).data('user-id');
        $(".form-detail-"+user_id).prop('disabled', false);
        $(".btn-update-"+user_id).show();
        $(".btn-cancel-"+user_id).show();
        $(".btn-edit-"+user_id).hide();
    });
    $(".btn-cancel").on('click',function(e){
        e.preventDefault();
        $(this).closest('form')[0].reset();
        var user_id = $(this).data('user-id');
        $(".form-detail-"+user_id).prop('disabled', true);
        $(".btn-update-"+user_id).hide();
        $(".btn-cancel-"+user_id).hide();
        $(".btn-edit-"+user_id).show();
    });
    $(".btn-update").on('click',function(e){
        e.preventDefault();
        var user_id = $(this).data('user-id');


        var post_data = {
                '_token': $('input[name=_token]').val(),
                'name': $('#form-detail-name-'+user_id).val(),
                'surname': $('#form-detail-surname-'+user_id).val(),
                'email': $('#form-detail-email-'+user_id).val(),
                'address_line': $('#form-detail-address_line-'+user_id).val(),
                'address_subdist': $('#form-detail-address_subdist-'+user_id).val(),
                'address_dist': $('#form-detail-address_dist-'+user_id).val(),
                'address_province': $('#form-detail-address_province-'+user_id).val(),
                'phone': $('#form-detail-phone-'+user_id).val(),
                'gender': $('#form-detail-gender-'+user_id).val(),
                'role': $('#form-detail-role-'+user_id).val(),
                'birthdate': $('#form-detail-birthdate-'+user_id).val(),
                'address_zipcode': $('#form-detail-address_zipcode-'+user_id).val(),
                'password': $('#form-detail-password-'+user_id).val(),
                'password_confirmation': $('#form-detail-password_confirmation-'+user_id).val(),
            };
        // if($('#form-detail-birthdate-'+user_id).val()!=''){
        //     post_data['birthdate'] = $('#form-detail-birthdate-'+user_id).val();    
        // }

        // if($('#form-detail-address_zipcode-'+user_id).val()!=''){
        //     post_data['address_zipcode'] = $('#form-detail-address_zipcode-'+user_id).val();    
        // }

        $.ajax({
            type: 'PUT',
            url: "{{route('admin.user.index')}}/"+user_id,
            data: post_data,
            success: function(data) {
                if(data.error){
                    var errorString = '';
                    $.each( data.error_detail, function( key, value) {
                        errorString += '\n\r' + value  ;
                    });
                    // errorString += '';
                    alert('พบข้อผิดพลาดในการแก้ไขข้อมูล'+errorString);
                }else{
                    $(".form-detail-"+data.user_detail.id).prop('disabled', true);
                    $(".btn-update-"+data.user_detail.id).hide();
                    $(".btn-cancel-"+data.user_detail.id).hide();
                    $(".btn-edit-"+data.user_detail.id).show();
                    alert('บันทึกข้อมูลสำเร็จ');

                    /** set new default  */
                    if(data.user_detail.birthdate!=null && data.user_detail.birthdate!=''){
                        date = new Date( data.user_detail.birthdate);
                        year = date.getFullYear();
                        month = date.getMonth()+1;
                        dt = date.getDate();
                        if (dt < 10) {
                        dt = '0' + dt;
                        }
                        if (month < 10) {
                        month = '0' + month;
                        }
                        $('#form-detail-birthdate-'+data.user_detail.id).attr('value',year+'-' + month + '-'+dt);
                    }
                    $('#form-detail-name-'+data.user_detail.id).attr('value',data.user_detail.name);
                    $('#form-detail-surname-'+data.user_detail.id).attr('value',data.user_detail.surname);
                    $('#form-detail-email-'+data.user_detail.id).attr('value',data.user_detail.email);
                    $('#form-detail-address_line-'+data.user_detail.id).attr('value',data.user_detail.address_line);
                    $('#form-detail-address_subdist-'+data.user_detail.id).attr('value',data.user_detail.address_subdist);
                    $('#form-detail-address_dist-'+data.user_detail.id).attr('value',data.user_detail.address_dist);
                    $('#form-detail-address_province-'+data.user_detail.id).attr('value',data.user_detail.address_province);
                    $('#form-detail-phone-'+data.user_detail.id).attr('value',data.user_detail.phone);
                    $('#form-detail-address_zipcode-'+data.user_detail.id).attr('value',data.user_detail.address_zipcode);
                    $('#form-detail-gender-'+data.user_detail.id+' option').each(function(){
                        $(this).attr('selected',false);
                        if($(this).val()==data.user_detail.gender){
                            $(this).attr('selected',"selected");
                        }
                    });
                    $('#form-detail-role-'+data.user_detail.id+' option').each(function(){
                        $(this).attr('selected',false);
                        if($(this).val()==data.user_detail.role){
                            $(this).attr('selected',"selected");
                        }
                    });
                    $('#form-detail-address_province-'+data.user_detail.id+' option').each(function(){
                        $(this).attr('selected',false);
                        if($(this).val()==data.user_detail.address_province){
                            $(this).attr('selected',"selected");
                        }
                    });
                    $('#form-detail-password-'+data.user_detail.id).val('');
                    $('#form-detail-password_confirmation-'+data.user_detail.id).val('');
                }
               // $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
            }
        });


    });
    
     $(".btn-delete").click(function (e) {
        var x = confirm("ยืนยันความต้องการลบข้อมูล");
        e.preventDefault();
        if (x) {
            var user_id = $(this).data('user-id');
            
        var post_data = {
                '_token': $('input[name=_token]').val()
            };
        $.ajax({
            type: 'delete',
            url: "{{route('admin.user.index')}}/"+user_id,
            data: post_data,
            success: function(data) {
                if(data.error){
                    alert('พบข้อผิดพลาดในการลบข้อมูล');
                }else{
                    alert('ลบข้อมูลสำเร็จ');
                    $(".tr-"+data.user_id).remove();
                }
            }
            });
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });

});
</script>
@endsection
