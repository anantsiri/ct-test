@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('alert-danger'))
                        <div class="alert alert-danger">
                            {{ session('alert-danger') }}
                        </div>
                    @endif
                    @auth
                    เข้าสู่ระบบเรียบร้อยแล้ว

                    <a class="btn btn-primary" href="{{ route('admin.user.index') }}">ไปยังระบบจัดการ user</a>
                    @else
                    <a href="{{ route('login') }}">เข้าสู่ระบบ</a>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
